from random import randint
from time import time

from kivy.app import App

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout

from kivy.factory import Factory
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup

import matplotlib.pyplot as plt
from matplotlib import style

import os

style.use('fivethirtyeight')
info = {}


class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    cancel = ObjectProperty(None)
    text_input = ObjectProperty(None)


class MainLayout(BoxLayout):

    @staticmethod
    def is_int(val):
        try:
            int(val)
            return True
        except ValueError:
            return False

    def sort(self):
        array = [int(x) for x in self.ids['input'].text.split() if MainLayout.is_int(x)]

        if not array:
            return

        beg = time()

        for i in range(1, len(array)):
            elem = array[i]
            j = i - 1
            while j > -1 and array[j] > elem:
                array[j + 1] = array[j]
                j -= 1
            array[j + 1] = elem

        sort_time = (time() - beg) * 10 ** 3

        info[len(array)] = sort_time

        self.ids['output'].text = ' '.join(str(x) for x in array)
        self.ids['time'].text = 'Time: ' + '{0:.6f}'.format(sort_time) + 'ms'

    def generate(self):
        try:
            number = int(self.ids['number'].text)
        except ValueError:
            return

        self.ids['input'].text = ' '.join(str(randint(-1000, 1000)) for x in range(number))

    def load(self, path, filename):
        with open(os.path.join(path, filename[0])) as f:
            self.ids['input'].text = f.read()

        self.dismiss_popup()

    def save(self, path, filename):
        with open(os.path.join(path, filename), 'w') as f:
            f.write(self.ids['output'].text)

        self.dismiss_popup()

    def dismiss_popup(self):
        self.popup.dismiss()

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self.popup = Popup(title="Load file", content=content, size_hint=(0.9, 0.9))
        self.popup.open()

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self.popup = Popup(title="Save file", content=content, size_hint=(0.9, 0.9))
        self.popup.open()

    def show_graph(self):
        xs = []
        ys = []
        for el_num in sorted(info.keys()):
            xs.append(el_num)
            ys.append(info[el_num])
        plt.plot(xs, ys)
        plt.show()


class Lab2App(App):
    def build(self):
        return MainLayout()


Factory.register('LoadDialog', cls=LoadDialog)
Factory.register('SaveDialog', cls=SaveDialog)

if __name__ == '__main__':
    Lab2App().run()
