from interpolation import Interpolation
from math import cos, log

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout


class MainLayout(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.interpolation = Interpolation(lambda x: x * cos(x + log(1 + x)), 1, 5, 10)
        self.mode = self.interpolation.lagrange

    def change_mode(self, mode):
        self.mode = self.interpolation.lagrange if mode == 0 else \
            self.interpolation.newton if mode == 1 else self.interpolation.aitken

    def count(self):
        try:
            x = float(self.ids['x_input'].text)
        except ValueError:
            return

        self.ids['result'].text = 'Result = ' + str(self.interpolation.calc_with_mistake(self.mode, x)[0])
        self.ids['mistake'].text = 'Mistake = ' + str(self.interpolation.calc_with_mistake(self.mode, x)[1])

    def build_graph(self):
        self.interpolation.build_graph(self.mode)

    def build_mistake_graph(self):
        self.interpolation.build_mistakes_graph(self.mode)


class Lab3App(App):
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    Lab3App().run()
