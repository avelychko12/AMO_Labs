from numpy import linspace
from matplotlib import pyplot as plt
from matplotlib import style


class Interpolation:
    def __init__(self, func, interval_beg, interval_end, step_count):
        self.xs = linspace(interval_beg, interval_end, step_count + 1)
        self.func = func

    def lagrange(self, x):
        z = 0

        for x_j in self.xs:
            p = 1

            for x_i in self.xs:
                if x_i != x_j:
                    p *= (x - x_i) / (x_j - x_i)

            z += self.func(x_j) * p

        return z

    def newton(self, x):
        z = 0

        def f(t):
            res = 0
            t += 1
            for x_j in self.xs[:t]:

                p = 1
                for x_i in self.xs[:t]:
                    if x_i != x_j:
                        p *= x_j - x_i

                res += self.func(x_j) / p
            return res

        p = 1

        for i in range(len(self.xs)):
            p *= x - self.xs[i - 1] if i > 0 else 1
            z += f(i) * p

        return z

    def aitken(self, x, beg=0, end=None):
        if end is None:
            end = len(self.xs) - 1

        x_beg = self.xs[beg]
        x_end = self.xs[end]

        return self.func(x_beg) if beg == end else \
            ((x - x_beg) * self.aitken(x, beg + 1, end) - (x - x_end) * self.aitken(x, beg, end - 1)) / (x_end - x_beg)

    def calc_with_mistake(self, method, x):
        real_value = self.func(x)
        interpolation_value = method(x)
        return interpolation_value, abs(interpolation_value - real_value)

    def build_graph(self, method):
        style.use('bmh')

        ys = [self.func(x) for x in self.xs]

        additional_range = (self.xs[-1] - self.xs[0]) * 0.35
        new_xs = linspace(self.xs[0] - additional_range, self.xs[-1] + additional_range * 2, len(self.xs) * 10)
        new_ys = [method(x) for x in new_xs]
        real_ys = [self.func(x) for x in new_xs]

        plt.plot(new_xs, real_ys, 'g:', self.xs, ys, 'ro', new_xs, new_ys)

        plt.grid(True)
        plt.show()

    def build_mistakes_graph(self, method):
        style.use('bmh')

        ys = [0 for x in self.xs]

        additional_range = (self.xs[-1] - self.xs[0])
        new_xs = linspace(self.xs[0], self.xs[-1], len(self.xs) * 10)
        new_ys = [self.calc_with_mistake(method, x)[1] for x in new_xs]

        plt.plot(self.xs, ys, 'ro', new_xs, new_ys)

        plt.grid(True)
        plt.show()
