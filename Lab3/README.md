# Lab3
## Інтерполяція функцій
Програма обчислює значення заданої функції y(i) = f(x(i)) у вузлах інтерполяції x[i] = a + h * i , де h =(b - a)/10, i = 0,1,...,10, на відрізку [a, b].
Реалізовані методи інтерполяції:
- многочлен Лагранжа
- многочлен Ньютона
- рекурентне співвідношення Ейткена

Використані технології:
- Pyhton 3.6
- Kivy - для побудови інтерфейсу
- Matplotlib - для побудови графіків
Програма кросплатформенна, тому може запускатися на Windows, Linux, MacOS.

### Результат роботи прогрми
![](http://i1250.photobucket.com/albums/hh525/zzzolll80/lab3_interface_zpsea273weu.png)

### Інтерпольований графік:
![](http://i1250.photobucket.com/albums/hh525/zzzolll80/lab3_interp_grpaph_zpsjlfo3i22.png)

### Граіфк похибок
![](http://i1250.photobucket.com/albums/hh525/zzzolll80/lab3_mistake_grpaph_zps7tn7t4tp.png)

