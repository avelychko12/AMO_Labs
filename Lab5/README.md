# Lab5
## Розв’язання систем лінійних алгебраїчних рівнянь
Програма ркалізує обчислення СЛАР методом Гауса з одиничними коефіцієнтами.

Використані технології:
- Pyhton 3.6
- Kivy - для побудови інтерфейсу
Програма кросплатформенна, тому може запускатися на Windows, Linux, MacOS, Android, iOS.

### Блок схема методу Гауса:
![](http://i1250.photobucket.com/albums/hh525/zzzolll80/lab5_flowchart_zps281vbbhe.png)

### Результат роботи прогрми
![](http://i1250.photobucket.com/albums/hh525/zzzolll80/lab5_interface_zpsdatsoryx.png)
