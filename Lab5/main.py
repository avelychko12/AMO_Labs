from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

from calculation import calc


class MainLayout(BoxLayout):
    def default_values(self):
        self.ids['a_input'].text = '1 -3 2\n3 -4 0\n2 -5 3'
        self.ids['b_input'].text = '1\n2\n2'

    def calculate(self):
        A = self.ids['a_input'].text.split('\n')
        B = self.ids['b_input'].text.split('\n')
        n = len(B)

        if n != len(A):
            self.ids['output'].text = 'Incorrect input!'
            return

        for i in range(n):
            A[i] = A[i].split(' ')
            if n != len(A[i]):
                self.ids['output'].text = 'Incorrect input!'
                return

        for i in range(n):
            for j in range(n):
                try:
                    A[i][j] = float(A[i][j])
                except ValueError:
                    self.ids['output'].text = 'Incorrect input!'
                    return

        for i in range(n):
            try:
                B[i] = float(B[i])
            except ValueError:
                self.ids['output'].text = 'Incorrect input!'
                return

        self.ids['output'].text = calc(A, B, n)


class Lab5App(App):
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    Lab5App().run()
