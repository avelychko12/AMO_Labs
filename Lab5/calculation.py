def calc(a, b, n):
    for k in range(n):
        a_max = a[k][k]
        l_col = k
        for i in range(k + 1, n):
            if a[i][k] > a_max:
                a_max = a[i][k]
                l_col = i

        if l_col != k:
            temp = a[l_col], b[l_col]
            a[l_col], b[l_col] = a[k], b[k]
            a[k], b[k] = temp

        try:
            for j in range(k + 1, n):
                a[k][j] /= a[k][k]

            b[k] /= a[k][k]
        except ZeroDivisionError:
            return 'Infinity'

        a[k][k] = 1

        for i in range(k + 1, n):
            m = a[i][k]
            for j in range(k, n):
                a[i][j] -= m * a[k][j]
            b[i] -= m * b[k]

    x = [0 for x in range(n)]
    x[-1] = b[-1]

    for i in range(n - 2, -1, -1):
        s = 0
        for j in range(i + 1, n):
            s += a[i][j] * x[j]
        x[i] = b[i] - s

    return '\n'.join(str(round(_x, 5)) for _x in x)
