from equation_calculator import NonlinearEquation

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

from kivy.uix.textinput import TextInput
from re import compile, sub


class FloatInput(TextInput):
    pat = compile('[^0-9]')

    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = sub(pat, '', substring)
        else:
            s = '.'.join([sub(pat, '', s) for s in substring.split('.', 1)])

        if substring[0] == '-' and (self.text == '' or self.text[0] != '-') and self.cursor_index() == 0:
            self.text = '-' + self.text

        if self.cursor_index() == 0 and self.text != '' and self.text[0] == '-':
            s = ''

        return super(FloatInput, self).insert_text(s, from_undo=from_undo)


class MainLayout(BoxLayout):
    def __init__(self):
        super(MainLayout, self).__init__()
        self.equation = NonlinearEquation(lambda x: x ** 3 + 2 * x - 4)

    def calculate(self):
        try:
            a = float(self.ids['a_input'].text)
            b = float(self.ids['b_input'].text)
            e = float(self.ids['e_input'].text)
        except ValueError:
            self.ids['output'].text = 'Incorrect input!'
            return

        if self.equation.find_answer(a, b, e) is None:
            self.ids['output'].text = 'Incorrect range!'
            return

        res, steps, info = self.equation.find_answer(a, b, e)

        self.ids['output'].text = 'Result: ' + str(res) + '\tStep count: ' + str(steps) + '\n\tAll steps:\n' + info


class Lab4App(App):
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    Lab4App().run()
