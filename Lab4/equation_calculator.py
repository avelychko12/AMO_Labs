class NonlinearEquation:
    def __init__(self, func):
        self.f = func

    def find_answer(self, a, b, e):
        if self.f(a) * self.f(b) > 0 or a >= b:
            return None

        k = 0

        full_info = ''

        while True:
            c = (b + a) / 2

            step_info = str(k + 1) + ':\t[' + str(a) + ' - ' + str(b) + '] -> ' + str(c) + '\n'
            full_info += step_info

            if abs(b - a) < e or self.f(c) == 0:
                return c, k + 1, full_info

            if self.f(a) * self.f(c) < 0:
                b = c
            else:
                a = c

            k += 1
